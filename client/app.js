var Anonymous = angular.module('Anonymous', ['ngRoute']);

Anonymous.config(function($routeProvider){
	$routeProvider.when('/', {
		controller:'ProjectsController',
		templateUrl: 'views/projects.html'
	})
	.when('/projects', {
		controller:'ProjectsController',
		templateUrl: 'views/projects.html'
	})
	.when('/projects/project_details/:id',{
		controller:'ProjectsController',
		templateUrl: 'views/project_details.html'
	})
	.when('/projects/add',{
		controller:'ProjectsController',
		templateUrl: 'views/add_project.html'
	})
	.when('/projects/edit/:id',{
		controller:'ProjectsController',
		templateUrl: 'views/edit_project.html'
	})
	.otherwise({
		redirectTo: '/'
	});
});