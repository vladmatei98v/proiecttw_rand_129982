var Anonymous = angular.module('Anonymous');

Anonymous.controller('ProjectsController', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams){
	
	console.log('ProjectsController loaded...');

	$scope.getProjects = function(){
		$http.get('/api/projects').then(function(response){
			$scope.projects = response.data;
		});
	}

	$scope.getProject = function(){
		var id = $routeParams.id;
		$http.get('/api/projects/'+id).then(function(response){
			$scope.project = response.data;
		});
	}

	$scope.addProject = function(){
		console.log($scope.project);
		$http.post('/api/projects/', $scope.project).then(function(response){
			window.location.href='#!/projects';
		});
	}

	$scope.updateProject = function(){
		var id = $routeParams.id;
		$http.put('/api/projects/'+id, $scope.project).then(function(response){
			window.location.href='#!/projects';
		});
	}

	$scope.removeProject = function(id){
		$http.delete('/api/projects/'+id).then(function(response){
			window.location.href='#!/projects';
		});
	}

}]);