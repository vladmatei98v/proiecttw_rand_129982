var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

// Setting the static directory for our app
app.use(express.static(__dirname+'/client'));

app.use(bodyParser.json());

Deliverable = require('./models/deliverable')
Project = require('./models/project')

// Connect to mongoose
mongoose.connect('mongodb://localhost/anonymous')
var db = mongoose.connection;

app.get('/', function(req, res){
	res.send('Hello Vlad! Please enter /api/projects');
});

app.get('/api/deliverables', function(req, res){
	Deliverable.getDeliverables(function(err, deliverables){
		if(err){
			throw err;
		}
		res.json(deliverables);
	})
});

app.post('/api/deliverables', function(req, res){
	var deliverable = req.body;
	Deliverable.addDeliverable(deliverable, function(err, deliverable){
		if(err){
			throw err;
		}
		res.json(deliverable);
	})
});

app.put('/api/deliverables/:_id', function(req, res){
	var id = req.params._id;
	var deliverable = req.body;
	Deliverable.updateDeliverable(id, deliverable, {}, function(err, deliverable){
		if(err){
			throw err;
		}
		res.json(deliverable);
	})
});

app.delete('/api/deliverables/:_id', function(req, res){
	var id = req.params._id;
	Deliverable.deleteDeliverable(id, function(err, deliverable){
		if(err){
			throw err;
		}
		res.json(deliverable);
	})
});

app.get('/api/projects', function(req, res){
	Project.getProjects(function(err, projects){
		if(err){
			throw err;
		}
		res.json(projects);
	})
});

app.get('/api/projects/:_id', function(req, res){
	Project.getProjectById(req.params._id, function(err, project){
		if(err){
			throw err;
		}
		res.json(project);
	})
});

app.post('/api/projects', function(req, res){
	var project = req.body;
	Project.addProject(project, function(err, project){
		if(err){
			throw err;
		}
		res.json(project);
	})
});

app.put('/api/projects/:_id', function(req, res){
	var id = req.params._id;
	var project = req.body;
	Project.updateProject(id, project, {}, function(err, project){
		if(err){
			throw err;
		}
		res.json(project);
	})
});

app.delete('/api/projects/:_id', function(req, res){
	var id = req.params._id;
	Project.deleteProject(id, function(err, project){
		if(err){
			throw err;
		}
		res.json(project);
	})
});

app.listen(1911);
console.log('Running on port 1911...')