var mongoose = require('mongoose');

// Deliverable schema
var deliverableSchema = mongoose.Schema({
	name:{
		type: String,
		required: true
	},
	type:{
		type: String
	},
	create_date:{
		type: Date,
		default: Date.now
	}
});

var Deliverable = module.exports = mongoose.model('Deliverable', deliverableSchema);

// Get Deliverables
module.exports.getDeliverables = function(callback, limit){
	Deliverable.find(callback).limit(limit);
}

// Add Deliverable
module.exports.addDeliverable = function(deliverable, callback){
	Deliverable.create(deliverable, callback);
}

// Update Deliverable
module.exports.updateDeliverable = function(id, deliverable, options, callback){
	var query = {_id: id};
	var update = {
		name: deliverable.name,
		type: deliverable.type
	}
	Deliverable.findOneAndUpdate(query, update, options, callback);
}

// Delete Deliverable
module.exports.deleteDeliverable = function(id, callback){
	var query = {_id: id};
	Deliverable.remove(query, callback);
}