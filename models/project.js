var mongoose = require('mongoose');

// Project schema
var projectSchema = mongoose.Schema({
	name:{
		type: String,
		required: true
	},
	create_date:{
		type: Date,
		default: Date.now
	}
});

var Project = module.exports = mongoose.model('Project', projectSchema);

// Get Projects
module.exports.getProjects = function(callback, limit){
	Project.find(callback).limit(limit);
}

// Get Project
module.exports.getProjectById = function(id, callback){
	Project.findById(id, callback);
}

// Add Project
module.exports.addProject = function(project, callback){
	Project.create(project, callback);
}

// Update Project
module.exports.updateProject = function(id, project, options, callback){
	var query = {_id: id};
	var update = {
		name: project.name
	}
	Project.findOneAndUpdate(query, update, options, callback);
}

// Delete Project
module.exports.deleteProject = function(id, callback){
	var query = {_id: id};
	Project.remove(query, callback);
}